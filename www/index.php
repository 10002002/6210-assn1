<!DOCTYPE html>
<html>
<title>Geyserland Accounting</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/style.css">
<header>
    <img src="images/logo.png" alt="Celtics" style="width:180px;height:180px;">
    <nav>
        <a href=index.php>Home</a>
        <a href=about.php>About</a>
        <a href=contact.php>Contact</a>
    </nav>
  </header>
  <main>
    <section>
      <article>
        <h2>Home</h2>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu justo velit. Duis vel tincidunt odio, eget commodo dolor. Etiam semper purus ac luctus luctus. Nulla auctor mauris arcu, sit amet elementum lacus pulvinar ac. Suspendisse consequat tempus sodales. Vivamus a lacus laoreet, convallis nunc at, mattis ex. Proin convallis, nisl at euismod mattis, nisl libero hendrerit eros, eu sollicitudin metus nisl nec dui. Maecenas auctor neque sed pellentesque sollicitudin. Cras eget imperdiet leo.

Vivamus nec lacus vel eros auctor suscipit in sit amet nisi. Suspendisse ante nisi, posuere et suscipit a, posuere quis enim. Aliquam sit amet vestibulum eros. Nunc porta scelerisque leo ut blandit. Quisque vel nibh nec odio maximus elementum in eu lacus. Phasellus ante diam, rhoncus eu nibh et, sagittis porta nulla. Donec fermentum viverra congue. Sed tincidunt leo sed massa viverra pretium. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla accumsan felis orci, ut lobortis est vulputate sit amet. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum ultrices mi eget porta. Nunc vehicula ante risus, congue tempus odio tincidunt at. Nam a libero a felis imperdiet viverra. Ut venenatis sollicitudin faucibus.

Maecenas nulla massa, volutpat quis lacinia egestas, sollicitudin ac lacus. Nullam vel turpis ac justo aliquet placerat non at felis. In eleifend lacinia arcu non vulputate. Sed sed est in mi facilisis malesuada. Nunc ligula risus, mollis a purus sit amet, ultrices malesuada justo. Nullam id facilisis nulla, et luctus justo. Nulla id volutpat odio. Mauris augue tortor, vehicula nec neque ut, condimentum viverra ipsum. Donec auctor eu nulla at dictum. Morbi vitae mattis lectus. Praesent lorem nibh, dapibus quis egestas ac, tincidunt a tortor. Quisque dignissim mi vel lacus tincidunt accumsan vel ac lorem. Nam nec neque est.

Sed posuere lorem sapien, et viverra nibh faucibus id. Pellentesque tortor enim, pellentesque quis faucibus in, convallis eu enim. Quisque at ultrices lorem. Cras congue placerat velit. Aenean et tempor dui, eget hendrerit massa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum luctus sapien lectus, id pulvinar magna dignissim in. Fusce ornare purus eu consectetur elementum. Aliquam malesuada enim arcu. Nunc sed tincidunt tellus. Donec nec risus fermentum, sollicitudin ante quis, hendrerit lectus. Praesent et volutpat risus, eu sodales nibh.

Ut orci lacus, gravida eget augue at, lobortis placerat sem. Suspendisse sit amet ullamcorper nulla. Aenean maximus massa nulla, et tempor sem sollicitudin quis. Praesent eget scelerisque metus. Duis tristique magna consequat massa ultrices, quis pellentesque magna fringilla. Etiam id tristique est. Nullam vel mauris eu magna vulputate aliquet ac id ligula. Nunc sed est vehicula, pellentesque magna vitae, aliquam magna. Etiam diam velit, vestibulum vel sodales nec, varius tincidunt neque. Nunc at hendrerit risus, id suscipit magna. Aliquam placerat lorem semper, cursus dolor in, suscipit orci. Donec id risus ligula. Maecenas egestas lobortis ligula, nec lacinia sapien varius in. Ut aliquet dolor at nisi ornare, at auctor enim ullamcorper. Integer varius nisi tincidunt, tempor tellus non, posuere metus.
      </article>
    </section>
    <aside>
      <div class=capsule>
        <h3>TBD</h3>
        <a class="twitter-timeline" href="https://twitter.com/GeyserIand?ref_src=twsrc%5Etfw">Tweets by GeyserIand</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
      </div>
    </aside>
  </main>
  <footer>&copy; Geyserland Accounting</footer>