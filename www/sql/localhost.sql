SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `comp6010_assn1` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `comp6010_assn1`;

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL,
  `PW` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`ID`, `NAME`, `PW`) VALUES
(1, 'root', 'password123');

DROP TABLE IF EXISTS `tbl_appointments`;
CREATE TABLE `tbl_appointments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FNAME` varchar(255) NOT NULL,
  `LNAME` varchar(255) NOT NULL,
  `TIME` double NOT NULL,
  `DATE` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_appointments` (`ID` `FNAME`, `LNAME`, `TIME`, `DATE`) VALUES
(1, 'LeBron' `James`, 12:45, '13th April'),
(2, 'Damian' 'Lillard', 2:30, '16th April'),
(3, 'Kyrie' 'Irving', 10:45, '17th April'),
(4, 'Kevin' 'Durant', 4:20, '18th April');
(5, 'Kawhi' 'Leonard', 5:15, '18th April');
(6, 'Russell' 'Westbrook', 10:30, '19th April');
(7, 'James' 'Harden', 11:15, '19th April');
(8, 'Giannis' 'Antetokounmpo', 1:00, '19th April');
(9, 'Anthony' 'Davis', 2:30, '19th April');
(10, 'Chris' 'Paul', 3:30, '20th April');